//
//  MenuController.h
//  NixieManager
//
//  Created by Emil Eriksson on 2012-09-10.
//  Copyright (c) 2012 Emil Eriksson. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "NetworkManager.h"

@interface MenuController : NSObject

@property (nonatomic) NetworkManager *networkManager;
@property (weak) IBOutlet NSMenu *menu;
@end
