//
//  NodeMenuController.h
//  NixieManager
//
//  Created by Emil Eriksson on 2012-09-10.
//  Copyright (c) 2012 Emil Eriksson. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Node.h"

@interface NodeMenuController : NSObject <NSMenuDelegate>

- (id)initWithNode:(Node *)n;

/**
 * Refreshes the menu with data from the node.
 */

/**
 * The node of the controller.
 */
@property (nonatomic, readonly) Node *node;

/**
 * The menu item.
 */
@property (nonatomic, readonly) NSMenuItem *menuItem;

@end
