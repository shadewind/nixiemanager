//
//  MenuController.m
//  NixieManager
//
//  Created by Emil Eriksson on 2012-09-10.
//  Copyright (c) 2012 Emil Eriksson. All rights reserved.
//

#import "MenuController.h"

#import "Node.h"
#import "NodeMenuController.h"

@interface MenuController ()
- (void)nodeDiscovered:(NSNotification *)notification;
- (void)addNodeMenuController:(NodeMenuController *)controller;
- (IBAction)refresh:(id)sender;
@property (weak) IBOutlet NSMenuItem *placeholderItem;
@property (weak) IBOutlet NSMenuItem *nodesSeparator;
@end

@implementation MenuController {
	NSMutableArray *nodeMenuControllers;
}

- (id)init
{
	self = [super init];
	if (self) {
		nodeMenuControllers = [NSMutableArray array];
	}
	
	return self;
}

- (void)setNetworkManager:(NetworkManager *)nm
{
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:@"nodeDiscovered"
												  object:_networkManager];
	
	_networkManager = nm;
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(nodeDiscovered:)
												 name:@"nodeDiscovered"
											   object:_networkManager];
}

- (void)nodeDiscovered:(NSNotification *)notification
{
	Node *node = notification.userInfo[@"node"];
	NodeMenuController *controller = [[NodeMenuController alloc] initWithNode:node];
	[self addNodeMenuController:controller];
}

- (void)addNodeMenuController:(NodeMenuController *)controller
{
	if (nodeMenuControllers.count == 0)
		[_menu removeItem:_placeholderItem];
	NSInteger insertIndex = [_menu indexOfItem:_nodesSeparator];
	[_menu insertItem:controller.menuItem atIndex:insertIndex];
	[nodeMenuControllers addObject:controller];
}

- (IBAction)refresh:(id)sender {
	[_networkManager refresh];
}

@end
