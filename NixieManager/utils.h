//
//  utils.h
//  NixieManager
//
//  Created by Emil Eriksson on 2012-09-09.
//  Copyright (c) 2012 Emil Eriksson. All rights reserved.
//

#ifndef NixieManager_utils_h
#define NixieManager_utils_h

#import <arpa/inet.h>

/**
 * Converts the specified IP address to a dotted string representation.
 *
 * @param addr  The address to convert.
 *
 * @return The string representation.
 */
NSString *addressToString(struct in_addr addr);

#endif
