//
//  Node.h
//  NixieManager
//
//  Created by Emil Eriksson on 2012-09-09.
//  Copyright (c) 2012 Emil Eriksson. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <netinet/in.h>

#import "NetworkManager.h"

@interface Node : NSObject

/**
 * Constructs a new \c Node.
 *
 * @param addr  The address of the node.
 * @param nm    The associated network manager.
 */
- (id)initWithAddress:(struct in_addr)addr networkManager:(NetworkManager *)nm;

/**
 * Gets the specified property from this node.
 *
 * @param name   The name of the property.
 * @param block  A block which is called on the main thread when the response is available.
 */
- (void)getProperty:(NSString *)name withBlock:(GetPropBlock)block;

/**
 * Sets the specified property to the specified value for this node.
 *
 * @param name   The name of the property.
 * @param value  The value to set.
 */
- (void)setProperty:(NSString *)name to:(NSString *)value;

/**
 * The associated network manager.
 */
@property (nonatomic, readonly) NetworkManager *networkManager;

/**
 * The address of the node.
 */
@property (nonatomic, readonly) struct in_addr address;

/**
 * The hostname of the node.
 */
@property (nonatomic, readonly) NSString *hostname;

@end
