//
//  AppDelegate.h
//  NixieManager
//
//  Created by Emil Eriksson on 2012-09-08.
//  Copyright (c) 2012 Emil Eriksson. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
