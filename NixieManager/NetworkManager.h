//
//  NetworkManager.h
//  NixieManager
//
//  Created by Emil Eriksson on 2012-09-08.
//  Copyright (c) 2012 Emil Eriksson. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^GetPropBlock)(NSString *);

@class Node;

/**
 * Handles network communication with nodes.
 */
@interface NetworkManager : NSObject

/**
 * Sends out a broadcast message to find new nodes.
 */
- (void)refresh;

/**
 * Reads a property from the specified node.
 *
 * @param name   The name of the property.
 * @param node   The node to read from.
 * @param block  The block to invoke when the response is ready.
 */
- (void)getProperty:(NSString *)name from:(Node *)node withBlock:(GetPropBlock)block;

/**
 * Sets a property for the specified node.
 *
 * @param name   The name of the property.
 * @param value  The value to set.
 * @param node   The node to set the property for.
 */
- (void)setProperty:(NSString *)name to:(NSString *)value forNode:(Node *)node;

@end
