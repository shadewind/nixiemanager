//
//  NetworkManager.m
//  NixieManager
//
//  Created by Emil Eriksson on 2012-09-08.
//  Copyright (c) 2012 Emil Eriksson. All rights reserved.
//

#import "NetworkManager.h"

#import <sys/socket.h>
#import <sys/ioctl.h>
#import <sys/sockio.h>
#import <net/if.h>
#import <netinet/in.h>
#import <ifaddrs.h>
#import <errno.h>

#import <dispatch/dispatch.h>

#import "Node.h"
#import "utils.h"

#define PORT 19191
#define MAX_CMD_LENGTH 256

@interface NetworkManager ()
- (void)receive;
- (void)broadcastCommand:(NSString *)cmd;
- (void)sendCommand:(NSString *)cmd toNode:(Node *)node;
- (void)sendCommand:(NSString *)cmd to:(struct sockaddr_in)saddr;
- (Node *)nodeForAddress:(struct in_addr)addr;
- (void)handleMessage:(NSString *)message forNode:(Node *)node;
- (void)handleGetprop:(NSString *)params forNode:(Node *)node;
- (void)nextRequest;
@end

@implementation NetworkManager {
	dispatch_queue_t queue;
	dispatch_source_t sockSource;
	int sock;
	char recvBuffer[MAX_CMD_LENGTH];
	NSMutableDictionary *nodes;
    
	NSMutableArray *requestQueue;
}

- (id)init
{
	self = [super init];
	if (self) {
		sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if (sock == -1) {
			NSLog(@"Unable to create socket.");
			return nil;
		}
		
		//Enable broadcast
		int broadcast = 1;
		if(setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof(broadcast)) != 0) {
			NSLog(@"Unable to set SO_BROADCAST.");
			close(sock);
			return nil;
		}
		
		struct sockaddr_in saddr;
		memset(&saddr, 0, sizeof(saddr));
		saddr.sin_len = sizeof(saddr);
		saddr.sin_family = AF_INET;
		saddr.sin_addr.s_addr = htonl(INADDR_ANY);
		saddr.sin_port = 0;
		if(bind(sock, (struct sockaddr *)&saddr, sizeof(saddr)) != 0) {
			NSLog(@"Unable to bind socket.");
			close(sock);
			return nil;
		}
		
		queue = dispatch_queue_create("network", DISPATCH_QUEUE_SERIAL);
		sockSource = dispatch_source_create(DISPATCH_SOURCE_TYPE_READ, sock, 0, queue);
		dispatch_source_set_event_handler(sockSource, ^{
			[self receive];
		});
		dispatch_source_set_cancel_handler(sockSource, ^{
			close(sock);
		});
		dispatch_resume(sockSource);
		
		nodes = [NSMutableDictionary dictionary];
		requestQueue = [NSMutableArray array];
	}
	
	return self;
}

- (void)refresh
{
	dispatch_async(queue, ^{
		[self broadcastCommand:@"ping"];
	});
}

- (void)getProperty:(NSString *)name from:(Node *)node withBlock:(void (^)(NSString *))block
{
    dispatch_async(queue, ^{
        NSDictionary *request = @{@"node" : node, @"propertyName" : name, @"block" : block};
        [requestQueue insertObject:request atIndex:0];
        
        if (requestQueue.count == 1)
            [self nextRequest];
    });
}

- (void)setProperty:(NSString *)name to:(NSString *)value forNode:(Node *)node
{
	NSString *cmd = [NSString stringWithFormat:@"setprop %@ %@", name, value];
	dispatch_async(queue, ^{
		[self sendCommand:cmd toNode:node];
	});
}

- (Node *)nodeForAddress:(struct in_addr)addr
{
	NSString *stringAddress = addressToString(addr);
	Node *node = [nodes objectForKey:stringAddress];
	if (node == nil) {
		node = [[Node alloc] initWithAddress:addr networkManager:self];
		[nodes setObject:node forKey:stringAddress];
		[[NSNotificationCenter defaultCenter] postNotificationName:@"nodeDiscovered" object:self userInfo:@{ @"node" : node }];
	}
	
	return node;
}

- (void)receive
{
	struct sockaddr_in saddr;
	socklen_t saddrSize = sizeof(saddr);
	int size = (int)recvfrom(sock, recvBuffer, MAX_CMD_LENGTH, 0, (struct sockaddr *)&saddr, &saddrSize);
	NSString *message = [[NSString alloc] initWithBytes:recvBuffer length:size encoding:NSASCIIStringEncoding];
	[self handleMessage:message forNode:[self nodeForAddress:saddr.sin_addr]];
}

- (void)handleMessage:(NSString *)message forNode:(Node *)node
{
	NSString *command = message;
	NSString *params = @"";
	NSRange range = [message rangeOfString:@" "];
	
	if (range.location != NSNotFound) {
		command = [message substringToIndex:range.location];
		params = [message substringFromIndex:range.location + 1];
	}
	
	if ([command isEqualToString:@"getprop"])
		[self handleGetprop:params forNode:node];
}

- (void)handleGetprop:(NSString *)params forNode:(Node *)node
{
	NSDictionary *request = [requestQueue lastObject];
	if (request[@"node"] == node) {
		dispatch_async(dispatch_get_main_queue(), ^{
			((GetPropBlock)request[@"block"])(params);
		});
		[requestQueue removeLastObject];
		[self nextRequest];
	}
}

- (void)sendCommand:(NSString *)cmd toNode:(Node *)node
{
    struct sockaddr_in destAddr;
    memset(&destAddr, 0, sizeof(destAddr));
    destAddr.sin_len = sizeof(destAddr);
    destAddr.sin_addr = node.address;
    destAddr.sin_family = AF_INET;
    destAddr.sin_port = htons(PORT);
    
    [self sendCommand:cmd to:destAddr];
}

- (void)sendCommand:(NSString *)cmd to:(struct sockaddr_in)saddr
{
	if(sendto(sock, [cmd cStringUsingEncoding:NSASCIIStringEncoding], cmd.length, 0, (struct sockaddr *)&saddr, sizeof(struct sockaddr_in)) == -1)
		NSLog(@"Unable to send command: %d", errno);
}

- (void)broadcastCommand:(NSString *)cmd
{
	struct ifaddrs *addrs;
	if (getifaddrs(&addrs) != 0) {
		NSLog(@"Unable to get interface addresses.");
		return;
	}
	
	for (struct ifaddrs *addr = addrs->ifa_next; addr->ifa_next != NULL; addr = addr->ifa_next) {
		if ((addr->ifa_dstaddr != NULL) && (addr->ifa_dstaddr->sa_family == AF_INET)) {
			struct sockaddr_in *ifaddr = (struct sockaddr_in *)addr->ifa_addr;
			//Skip loopback
			if(ntohl(ifaddr->sin_addr.s_addr) == INADDR_LOOPBACK)
				continue;
			
			struct sockaddr_in destAddr = *((struct sockaddr_in *)addr->ifa_dstaddr);
			destAddr.sin_port = htons(PORT);
			[self sendCommand:cmd to:destAddr];
		}
	}
	
	freeifaddrs(addrs);
}

- (void)nextRequest
{
	if (requestQueue.count == 0)
		return;
	
    NSDictionary *request = [requestQueue lastObject];
    NSString *cmd = [NSString stringWithFormat:@"getprop %@", request[@"propertyName"]];
    [self sendCommand:cmd toNode:request[@"node"]];
}

- (void)dealloc
{
	dispatch_source_cancel(sockSource);
}

@end
