//
//  NodeMenuController.m
//  NixieManager
//
//  Created by Emil Eriksson on 2012-09-10.
//  Copyright (c) 2012 Emil Eriksson. All rights reserved.
//

#import "NodeMenuController.h"

#import "ConfigureWindowController.h"

@interface NodeMenuController ()
- (IBAction)openConfigure:(id)sender;
@property (weak) IBOutlet NSMenuItem *nodeDescriptionItem;
@property (strong) IBOutlet NSMenu *menu;
@end

@implementation NodeMenuController

- (id)initWithNode:(Node *)n
{
	self = [super init];
	if (self) {
		_node = n;
		[NSBundle loadNibNamed:@"NodeMenu" owner:self];
		_menuItem = [[NSMenuItem alloc] init];
		_menuItem.submenu = _menu;
	}
	
	return self;
}

- (IBAction)openConfigure:(id)sender {
	ConfigureWindowController *configWinController =
		[[ConfigureWindowController alloc] initWithWindowNibName:@"ConfigureWindow"];
	configWinController.node = _node;
	[NSApp runModalForWindow:configWinController.window];
	
	[_node getProperty:@"name" withBlock:^(NSString *value) {
		_menuItem.title = value;
	}];
}

- (void)menuNeedsUpdate:(NSMenu *)menu
{
	_nodeDescriptionItem.title = [NSString stringWithFormat:@"Host: %@", _node.hostname];
}

@end
