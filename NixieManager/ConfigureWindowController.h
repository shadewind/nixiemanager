//
//  ConfigureWindowController.h
//  NixieManager
//
//  Created by Emil Eriksson on 1/22/13.
//  Copyright (c) 2013 Emil Eriksson. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class Node;

@interface ConfigureWindowController : NSWindowController

@property (nonatomic) NSString *name;
@property (nonatomic) NSString *ntpServer;
@property int ntpSyncInterval;
@property (nonatomic) NSString *timezoneString;

/**
 * The node.
 */
@property (nonatomic) Node *node;

@end
