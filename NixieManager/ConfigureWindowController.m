//
//  ConfigureWindowController.m
//  NixieManager
//
//  Created by Emil Eriksson on 1/22/13.
//  Copyright (c) 2013 Emil Eriksson. All rights reserved.
//

#import "ConfigureWindowController.h"

#import "Node.h"

@interface ConfigureWindowController ()
- (IBAction)cancel:(id)sender;
- (IBAction)ok:(id)sender;
@end

@implementation ConfigureWindowController

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

- (void)setNode:(Node *)node
{
	_node = node;
	
	[node getProperty:@"name" withBlock:^(NSString *value) {
		self.name = value;
	}];

	[node getProperty:@"ntpServer" withBlock:^(NSString *value) {
		self.ntpServer = value;
	}];
	
	[node getProperty:@"ntpSyncInterval" withBlock:^(NSString *value) {
		self.ntpSyncInterval = [value intValue] / 60;
	}];
	
	[node getProperty:@"tzString" withBlock:^(NSString *value) {
		self.timezoneString = value;
	}];
}

- (IBAction)cancel:(id)sender {
	[NSApp stopModal];
}

- (IBAction)ok:(id)sender {
	[_node setProperty:@"name" to:_name];
	[_node setProperty:@"ntpServer" to:_ntpServer];
	[_node setProperty:@"ntpSyncInterval" to:[NSString stringWithFormat:@"%d", _ntpSyncInterval * 60]];
	[_node setProperty:@"tzString" to:_timezoneString];
	[NSApp stopModal];
}
@end
