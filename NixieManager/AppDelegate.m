//
//  AppDelegate.m
//  NixieManager
//
//  Created by Emil Eriksson on 2012-09-08.
//  Copyright (c) 2012 Emil Eriksson. All rights reserved.
//

#import "AppDelegate.h"

#import "NetworkManager.h"
#import "MenuController.h"

@interface AppDelegate ()
@property (weak) IBOutlet MenuController *menuController;
@end

@implementation AppDelegate {
	NSStatusItem *statusItem;
	NetworkManager *networkManager;
}
@synthesize menuController;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	networkManager = [[NetworkManager alloc] init];
	menuController.networkManager = networkManager;

	statusItem = [[NSStatusBar systemStatusBar] statusItemWithLength:NSVariableStatusItemLength];
	statusItem.title = @"N";
	statusItem.menu = menuController.menu;
	statusItem.highlightMode = YES;
	
	[networkManager refresh];
}

@end
