//
//  utils.m
//  NixieManager
//
//  Created by Emil Eriksson on 2012-09-09.
//  Copyright (c) 2012 Emil Eriksson. All rights reserved.
//

#import "utils.h"

NSString *addressToString(struct in_addr addr)
{
	char buf[INET_ADDRSTRLEN];
	inet_ntop(AF_INET, &addr, buf, INET_ADDRSTRLEN);
	return [NSString stringWithCString:buf encoding:NSASCIIStringEncoding];
}