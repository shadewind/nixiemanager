//
//  main.m
//  NixieManager
//
//  Created by Emil Eriksson on 2012-09-08.
//  Copyright (c) 2012 Emil Eriksson. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
	return NSApplicationMain(argc, (const char **)argv);
}
