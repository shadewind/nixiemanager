//
//  Node.m
//  NixieManager
//
//  Created by Emil Eriksson on 2012-09-09.
//  Copyright (c) 2012 Emil Eriksson. All rights reserved.
//

#import "Node.h"

#import "utils.h"

@implementation Node {
	struct in_addr address;
}

@synthesize address;
@synthesize networkManager;

- (id)initWithAddress:(struct in_addr)addr networkManager:(NetworkManager *)nm;
{
	self = [super init];
	if (self)
		address = addr;
		networkManager = nm;
	
	return self;
}

- (void)getProperty:(NSString *)name withBlock:(void (^)(NSString *value))block
{
    [networkManager getProperty:name from:self withBlock:block];
}

- (void)setProperty:(NSString *)name to:(NSString *)value
{
	[networkManager setProperty:name to:value forNode:self];
}

- (NSString *)hostname
{
	return addressToString(address);
}

@end
